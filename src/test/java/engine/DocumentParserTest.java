package engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import com.qiangzi.workflow.engine.bpmn.base.ProcessDefinition;
import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;
import com.qiangzi.workflow.engine.core.MyEngine;

public class DocumentParserTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(DocumentParserTest.class);

    public static void main(String[] args) throws Exception {

        // 引擎---真正使用是通过spring来使用MyEngine单例
        MyEngine engine = new MyEngine();

        // 解析
        Resource resource = null;

        resource = new DefaultResourceLoader().getResource("engine.xml");

        ProcessDefinition processDefinition = engine.parse(resource);

        for (int i = 0; i < 1; i++) {
            LOGGER.info("------------------------------");
            // 部署-每个请求应该触发1次
            ProcessInstance processInstance = engine.deploy(processDefinition);
            // 运行这个流程实例-1个请求对应1个实例
            engine.run(processInstance);
        }

    }

}
