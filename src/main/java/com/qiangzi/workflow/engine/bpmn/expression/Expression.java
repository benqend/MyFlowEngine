package com.qiangzi.workflow.engine.bpmn.expression;

import org.dom4j.Attribute;
import org.dom4j.Element;
import org.springframework.util.Assert;

public abstract class Expression {

	private String type;

	private String expression;

	public void setType(String type) {
		this.type = type;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getType() {
		return type;
	}

	public String getExpression() {
		return expression;
	}

	public void parse(Element element) {

		Assert.isTrue(element != null, "element is null");

		// type属性必须存在
		Attribute typeAttribute = element.attribute("type");
		Assert.isTrue(null != typeAttribute, "typeAttribute is null");
		String type = typeAttribute.getText();
		Assert.isTrue(null != type && type.equals("juel"), "type value must be juel");
		this.type = type;

		// expression必须存在
		String expression = element.getText();
		Assert.hasText(expression, "expression notn valid");
		// 去掉\n\t
		expression = expression.replaceAll("\n", "");
		expression = expression.replaceAll("\t", "");
		Assert.hasText(expression, "expression notn valid");
		this.expression = expression;

	}
}
