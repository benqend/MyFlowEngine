package com.qiangzi.workflow.engine.bpmn.event;

import org.dom4j.Element;

public class EndEvent extends Event {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	public Object copy() {
		EndEvent endEvent = new EndEvent();
		copy(endEvent);
		return endEvent;
	}

	protected void copy(Object obj) {
		super.copy(obj);
	}

}
