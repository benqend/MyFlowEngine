package com.qiangzi.workflow.engine.bpmn.base;

public class State {
	/**
	 * 新创建
	 */
	public static int NEW = 1;
	/**
	 * 确实触发执行了,注意!!!---可以表示一个点被执行,也可以表示一个边被执行
	 */
	public static int INVOKED = 2;
	/**
	 * 比如说经过判断,不会触发执行了,注意!!!---可以表示一个点被取消,也可以表示一个边被取消
	 */
	public static int IGNORED = 4;

}
